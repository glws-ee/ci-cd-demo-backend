stages:
  - lint
  - deploy
  - review

default:
  image: node:14.17.0-alpine
  cache:
    key:
      files:
        - package-lock.json
    paths:
      - .npm/
  before_script:
    - npm ci --cache .npm --prefer-offline

.setup_ssh:
  before_script:
    - 'which ssh-agent || (apk add --update openssh curl bash git)'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts

eslint:
  stage: lint
  script:
    - npm run eslint

prettier:
  stage: lint
  script:
    - npm run prettier:check

deploy:
  extends:
    - .setup_ssh
  stage: deploy
  variables:
    REPOSITORY_URL: 'git@gitlab.com:ajololr/ci-cd-demo-backend.git'
    DEPLOY_BRANCH: $CI_COMMIT_REF_NAME
    DEPLOY_DST: '/home/deploy/demo/$CI_COMMIT_REF_SLUG'
    APP_NAME: $CI_COMMIT_REF_SLUG
  script:
    - npm i -g pm2
    - ssh -q $SSH_USER@$SSH_HOST [[ ! -d "$DEPLOY_DST" ]] && pm2 deploy ecosystem.config.js production setup
    - |
      ssh -q $SSH_USER@$SSH_HOST <<EOF
        cd $DEPLOY_DST/current
        npm install
        pm2 delete --silent $APP_NAME
        env APP_NAME=$APP_NAME PORT=3000 pm2 start ecosystem.config.js --env production --update-env
      EOF
  environment:
    name: production
    url: http://$APP_HOST/api
    deployment_tier: production
  rules:
    - if: $CI_COMMIT_BRANCH == 'main'

create_review_app:
  extends:
    - .setup_ssh
  stage: review
  variables:
    REPOSITORY_URL: 'git@gitlab.com:ajololr/ci-cd-demo-backend.git'
    DEPLOY_DST: '/home/deploy/demo/$CI_COMMIT_REF_SLUG'
    DEPLOY_BRANCH: $CI_COMMIT_REF_NAME
    APP_NAME: $CI_COMMIT_REF_SLUG
    BRANCH_EXISTS_URL: 'https://gitlab.com/api/v4/projects/$FRONTEND_PROJECT_ID/repository/branches/$CI_COMMIT_REF_NAME'
    TRIGGER_PIPELINE_URL: 'https://gitlab.com/api/v4/projects/$FRONTEND_PROJECT_ID/trigger/pipeline'
  script:
    - npm i -g pm2
    - ssh -q $SSH_USER@$SSH_HOST [[ ! -d "$DEPLOY_DST" ]] && pm2 deploy ecosystem.config.js production setup
    - PORT=$(ssh -q $SSH_USER@$SSH_HOST ./getAvailablePort.sh)
    - |
      ssh -q $SSH_USER@$SSH_HOST <<EOF
        cd $DEPLOY_DST/current
        npm install
        pm2 delete --silent $APP_NAME
        env APP_NAME=$APP_NAME PORT=$PORT pm2 start ecosystem.config.js --env production --update-env
        ~/generateNginxConf.sh $APP_NAME $PORT
      EOF
    - 'status_code=$(curl -I --write-out "%{http_code}" --silent --output /dev/null "$BRANCH_EXISTS_URL")'
    - 'ref_name=$(if [[ "$status_code" -ne 204 ]]; then echo main; else echo $CI_COMMIT_REF_NAME; fi)'
    - curl --request POST --form token=$REVIEW_TRIGGER_TOKEN --form ref=$ref_name --form "variables[APP_NAME]=$APP_NAME" "$TRIGGER_PIPELINE_URL"
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: http://$CI_COMMIT_REF_SLUG.$APP_HOST/api
    deployment_tier: development
    on_stop: stop_review
  rules:
    - if: '$CI_MERGE_REQUEST_TITLE =~ /SKIP REVIEW/'
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

stop_review:
  stage: review
  extends:
    - .setup_ssh
  variables:
    APP_NAME: $CI_COMMIT_REF_SLUG
  script:
    - |
      ssh $SSH_USER@$SSH_HOST <<EOF
        pm2 delete APP_NAME
      EOF
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
  rules:
    - if: '$CI_MERGE_REQUEST_TITLE =~ /SKIP REVIEW/'
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual
